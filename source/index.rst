.. project documentation master file, created by
   sphinx-quickstart on Tue Nov 19 14:30:59 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to project's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
.. automodule:: math_lib
   :members:
